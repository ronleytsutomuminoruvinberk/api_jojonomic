<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'user'], function () {
    Route::post('login', 'API\UserController@login');
    Route::post('register', 'API\UserController@register');
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::group(['prefix' => 'master_data'], function () {
        Route::get('companies', 'API\CompanyController@index');
        Route::post('companies', 'API\CompanyController@store');
    });

    Route::group(['prefix' => 'document_services'], function () {
        Route::get('/', 'API\DocumentServiceController@index');
        Route::post('/', 'API\DocumentServiceController@store');
        Route::put('/{document_service_id}', 'API\DocumentServiceController@update');

        Route::get('files', 'API\FileController@index');
        Route::post('files', 'API\FileController@store');
        Route::get('files/{file_id}', 'API\FileController@show');
        Route::put('files/{file_id}', 'API\FileController@update');
        Route::delete('files/{file_id}', 'API\FileController@destroy');

        Route::get('folders', 'API\FolderController@index');
        Route::post('folders', 'API\FolderController@store');
        Route::put('folders/{folder_id}', 'API\FolderController@update');
        Route::delete('folders/{folder_id}', 'API\FolderController@destroy');
    });
});
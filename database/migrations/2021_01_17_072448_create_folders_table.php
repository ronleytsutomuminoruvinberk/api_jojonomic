<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoldersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('folders', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('owner_id', false)->nullable();
            $table->integer('company_id', false)->nullable();
            $table->string('name', 100);
            $table->string('type', 6)->nullable();
            $table->string('is_public', 1)->nullable();
            $table->foreign('owner_id')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('folders');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_services', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('file_id')->nullable();
            $table->integer('folder_id')->nullable();
            $table->foreign('file_id')->references('id')->on('files')->onUpdate('cascade');
            $table->foreign('folder_id')->references('id')->on('folders')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_services');
    }
}

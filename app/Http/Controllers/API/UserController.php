<?php

namespace App\Http\Controllers\API;

use Validator;

use Carbon\Carbon;

use App\User;

use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\API\APITemplateController as APITemplate;

class UserController extends APITemplate
{
    public function login(Request $request, User $user)
    {
        $request_input = $request->only("email", "password");

        $validator = Validator::make($request_input, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError(400, $validator->errors());
        } 
        
        try {
            if (Auth::attempt($request->only("email", "password"))) {
                $user = $request->user();
                $tokenResult = $user->createToken('Jm7tDy2vFs0gcqiKFwmCpIz1ew3bFyR2keuSvLx7');

                $data = [
                    'uuid' => $user->uuid,
                    'access_token' => $tokenResult->accessToken,
                    'token_type' => 'Bearer',
                    'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
                ];
                return $this->sendResponse(201, $data);
            }
            else {
                throw new Exception('These credentials do not match our records. Please try again!');
            }
        } catch (Exception $err) {
            return $this->sendError(500, $err->getMessage());
        }
    }

    public function register(Request $request, User $user)
    {
        $request_input = $request->only("email", "password", 'c_password');

        $validator = Validator::make($request_input, [
            'password' => 'required',
            'email' => 'required|email|unique:users',
            'c_password' => 'required|same:password'
        ]);

        if ($validator->fails()) {
            return $this->sendError(400, $validator->errors());
        }

        $request_input['password'] = Hash::make($request_input['password']);
        $request_input['uuid'] = Str::uuid();
        
        try {
            if (User::create($request_input)) {
                return $this->sendResponse(201, 'Email ' . $request_input['email'] . ' has been successfully register');
            }
            else {
                throw new Exception('Email can\'t registered. Please try again!');
                
            }
        } catch (Exception $err) {
            return $this->sendError(500, $err->getMessage());
        }
    }
}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;

class APITemplateController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($code = '', $result = [], $message = '')
    {
        if ($result == '') {
            $response = [
                'code' => $code,
                'message' => $message
            ];
        }
        elseif ($message == '') {
            $response = [
                'code' => $code,
                'data'  => $result,
            ];
        }
        else {
            $response = [
                'code' => $code,
                'message' => $message,
                'data'  => $result,
            ];
        }
        return response()->json($response);
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($code = '', $messages = [])
    {
    	$response = [
            'code' => $code,
            'message' => $messages,
        ];
        return response()->json($response);
    }
}

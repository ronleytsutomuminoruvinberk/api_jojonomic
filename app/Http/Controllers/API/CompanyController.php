<?php

namespace App\Http\Controllers\API;

use Validator;

use Illuminate\Http\Request;

use App\Models\Company;
use App\Http\Controllers\API\APITemplateController as APITemplate;

class CompanyController extends APITemplate
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Company::all();
        return $this->sendResponse(200, $result->toArray());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request_input = $request->only("name", "email", 'phone', 'address');

        $validator = Validator::make($request_input, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError(400, $validator->errors());
        }

        try {
            if (Company::create($request_input)) {
                return $this->sendResponse(201, 'Data company of ' . $request_input['name'] . ' has successfully saved');
            }
            else {
                throw new Exception('Data company of ' . $request_input['name'] . ' can\'t saved. Please try again!');
            }
        } catch (Exception $err) {
            return $this->sendError(500, $err->getMessage());
        }
    }
}

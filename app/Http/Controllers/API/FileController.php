<?php

namespace App\Http\Controllers\API;

use DB;

use Validator;

use App\Models\File;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\API\APITemplateController as APITemplate;

class FileController extends APITemplate
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = File::all();
        return $this->sendResponse(200, $result->toArray());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, File $file)
    {
        $request_input = $request->only('owner_id', 'name', 'type', 'content');

        $validator = Validator::make($request_input, [
            'owner_id' => 'nullable',
            'company_id' => 'nullable',
            'name' => 'required',
            'type' => 'nullable',
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError(400, $validator->errors());
        }

        $request_input['type'] = 'file';
        $request_input['owner_id'] = Auth::user()->id;
        $company_id = DB::table('companies')->get()->toArray();
        $request_input['company_id'] = $company_id[0]->id;

        try {
            if ($file::create($request_input)) {
                return $this->sendResponse(201, array(
                    'owner_id' => $request_input['owner_id'],
                    'company_id' => $request_input['company_id'],
                    'type' => $request_input['type'],
                    'name' => $request_input['name'],
                    'content' => $request_input['content'],
                ), 'File has been successfully created');
            }
            else {
                throw new Exception('File can\'t created. Please try again!');
            }
        } catch (Exception $err) {
            return $this->sendError(500, $err->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function show(File $file, $id)
    {
        $file_result = $file::findOrFail($id);
        return $this->sendResponse(200, array('file' => $file_result->toArray()), 'Success get file');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, File $file, $id)
    {
        $file_result = $file::findOrFail($id);

        $request_input = $request->only('owner_id', 'name', 'type', 'content');

        $validator = Validator::make($request_input, [
            'owner_id' => 'nullable',
            'folder_id' => 'nullable',
            'company_id' => 'nullable',
            'name' => 'required',
            'type' => 'nullable',
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError(400, $validator->errors());
        }

        $request_input['type'] = 'file';
        $request_input['owner_id'] = Auth::user()->id;
        $company_id = DB::table('companies')->get()->toArray();
        $request_input['company_id'] = $company_id[0]->id;

        try {
            if ($file_result->update($request_input)) {
                return $this->sendResponse(201, array(
                    'owner_id' => $request_input['owner_id'],
                    'company_id' => $request_input['company_id'],
                    'type' => $request_input['type'],
                    'name' => $request_input['name'],
                    'content' => $request_input['content'],
                ), 'File has been successfully updated');
            }
            else {
                throw new Exception('File can\'t updated. Please try again!');
            }
        } catch (Exception $err) {
            return $this->sendError(500, $err->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $file, $id)
    {
        try {
            $file_result = $file::findOrFail($id);
            if ($file_result->delete()) {
                return $this->sendResponse(201, 'File has been successfully deleted');
            }
            else {
                throw new Exception('File can\'t deleted. Please try again!');
            }
        } catch (Exception $err) {
            return $this->sendError(500, $err->getMessage());
        }
    }
}

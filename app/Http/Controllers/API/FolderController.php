<?php

namespace App\Http\Controllers\API;

use DB;

use Validator;

use App\Models\Folder;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\API\APITemplateController as APITemplate;

class FolderController extends APITemplate
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Folder::all();
        return $this->sendResponse(200, $result->toArray());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Folder $folder)
    {
        $request_input = $request->only('owner_id', 'company_id', 'name', 'type');

        $validator = Validator::make($request_input, [
            'owner_id' => 'nullable',
            'company_id' => 'nullable',
            'name' => 'required',
            'type' => 'nullable'
        ]);

        if ($validator->fails()) {
            return $this->sendError(400, $validator->errors());
        }

        $folder_result = DB::table('folders')->where('name', '=', $request_input['name'])->get();

        if ($folder_result->count() > 0) {
            return $this->sendError(400, 'Folder\'s is exists');
        }
        
        $request_input['type'] = 'folder';
        $request_input['owner_id'] = Auth::user()->id;
        $company_id = DB::table('companies')->get()->toArray();
        $request_input['company_id'] = $company_id[0]->id;

        try {
            if ($folder::create($request_input)) {
                return $this->sendResponse(201, array(
                    'owner_id' => $request_input['owner_id'],
                    'company_id' => $request_input['company_id'],
                    'type' => $request_input['type'],
                    'name' => $request_input['name'],
                ), 'Folder has been successfully created');
            }
            else {
                throw new Exception('Folder can\'t created. Please try again!');
            }
        } catch (Exception $err) {
            return $this->sendError(500, $err->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Folder  $folder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Folder $folder, $id)
    {
        $folder_result = $folder::findOrFail($id);

        $request_input = $request->only('owner_id', 'company_id', 'name', 'type');

        $validator = Validator::make($request_input, [
            'owner_id' => 'nullable',
            'company_id' => 'nullable',
            'name' => 'required',
            'type' => 'nullable'
        ]);

        if ($validator->fails()) {
            return $this->sendError(400, $validator->errors());
        }
        
        $request_input['type'] = 'folder';
        $request_input['owner_id'] = Auth::user()->id;
        $company_id = DB::table('companies')->get()->toArray();
        $request_input['company_id'] = $company_id[0]->id;

        try {
            if ($folder_result->update($request_input)) {
                return $this->sendResponse(201, array(
                    'owner_id' => $request_input['owner_id'],
                    'company_id' => $request_input['company_id'],
                    'type' => $request_input['type'],
                    'name' => $request_input['name'],
                ), 'Folder has been successfully updated');
            }
            else {
                throw new Exception('Folder can\'t updated. Please try again!');
            }
        } catch (Exception $err) {
            return $this->sendError(500, $err->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Folder  $folder
     * @return \Illuminate\Http\Response
     */
    public function destroy(Folder $folder, $id)
    {
        try {
            $folder_result = $folder::findOrFail($id);
            if ($folder_result->delete()) {
                return $this->sendResponse(201, 'Folder has been successfully deleted');
            }
            else {
                throw new Exception('Folder can\'t deleted. Please try again!');
            }
        } catch (Exception $err) {
            return $this->sendError(500, $err->getMessage());
        }
    }
}

<?php

namespace App\Http\Controllers\API;

use DB;

use Validator;

use App\Models\File;
use App\Models\Folder;
use App\Models\DocumentService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\API\APITemplateController as APITemplate;

class DocumentServiceController extends APITemplate
{
    public function index(Request $request)
    {
        $request_input = $request->only('is_public');

        $result = DB::table('document_services')->select(
            'files.name AS file_name',
            'files.type AS file_type',

            'folders.name AS folder_name',
            'folders.type AS folder_type',

            'users.email AS user_email',
            'other_users.email AS other_user_email',

            'document_services.user_id',
            'document_services.other_user_id'
        )
        ->leftjoin('files', 'document_services.file_id', '=', 'files.id')
        ->leftjoin('folders', 'document_services.folder_id', '=', 'folders.id')
        ->leftjoin('users', 'document_services.user_id', '=', 'users.id')
        ->leftjoin('users AS other_users', 'document_services.other_user_id', '=', 'other_users.id')
        ->where('document_services.is_public', '=', $request_input['is_public'])->get();
        return $this->sendResponse(200, $result->toArray());
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, DocumentService $document_service, File $file, Folder $folder)
    {
        $request_input = $request->only('file_id', 'folder_id', 'user_id', 'other_user_id', 'is_public');

        $validator = Validator::make($request_input, [
            'file_id' => 'nullable',
            'user_id' => 'nullable',
            'folder_id' => 'nullable',
            'is_public' => 'nullable',
            'other_user_id' => 'nullable'
        ]);

        if ($validator->fails()) {
            return $this->sendError(400, $validator->errors());
        }

        $request_input['user_id'] = Auth::user()->id;

        try {
            if ($document_service::create($request_input)) {
                return $this->sendResponse(201, 'Document has been successfully created');
            }
            else {
                throw new Exception('Document can\'t created. Please try again!');
            }
        } catch (Exception $err) {
            return $this->sendError(500, $err->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Folder  $folder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DocumentService $document_service, File $file, Folder $folder, $id)
    {
        $document_service_result = $document_service::findOrFail($id);

        $request_input = $request->only('file_id', 'folder_id', 'user_id', 'other_user_id', 'is_public');

        $validator = Validator::make($request_input, [
            'file_id' => 'nullable',
            'user_id' => 'nullable',
            'folder_id' => 'nullable',
            'is_public' => 'nullable',
            'other_user_id' => 'nullable'
        ]);

        if ($validator->fails()) {
            return $this->sendError(400, $validator->errors());
        }

        $request_input['user_id'] = Auth::user()->id;

        try {
            if ($document_service_result->update($request_input)) {
                return $this->sendResponse(201, 'Document has been successfully updated');
            }
            else {
                throw new Exception('Document can\'t updated. Please try again!');
            }
        } catch (Exception $err) {
            return $this->sendError(500, $err->getMessage());
        }
    }
}

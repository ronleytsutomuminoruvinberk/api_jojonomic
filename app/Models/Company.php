<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'name', 'email', 'phone', 'address', 'created_at', 'created_by', 'updated_at', 'updated_by'
    ];
    
    public $timestamps = true;
}

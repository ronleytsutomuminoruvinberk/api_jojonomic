<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentService extends Model
{
    protected $fillable = [
        'file_id', 'folder_id', 'user_id', 'other_user_id', 'is_public', 'created_at', 'created_by', 'updated_at', 'updated_by'
    ];
    
    public $timestamps = true;
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = [
        'owner_id', 'company_id', 'name', 'type', 'content', 'created_at', 'created_by', 'updated_at', 'updated_by'
    ];
    
    public $timestamps = true;
}
